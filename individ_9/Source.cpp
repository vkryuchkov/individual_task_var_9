#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <vector>
#include <sstream>
#include <fstream>
#include <ctime>

using namespace std;

typedef struct Service{
	string name;
	unsigned int code;
	float tariff;
	string time_span;
};

typedef struct Clients{
	string fio;
	string number;
	string date_conclusion_contract;
	string date_expired_contract;
};

typedef struct Stats{
	string number;
	unsigned int code;
	string date_and_time_of_use;
	unsigned int time;
};

vector<Service> services;
vector<Stats> stats;
vector<Clients> clients;

vector<string> split(string str, string del) {
	vector<string> sv;
	stringstream ss(str);
	string item;
	while (getline(ss, item, del[0])) {
		sv.push_back(item);
	}
	return sv;
}

void getClients(){
	string l;
	ifstream ifs("clients.txt");
	vector<string> strs;
	Clients cl;
	while (!ifs.eof()){
		getline(ifs, l);
		strs = split(l, ",");
		cl.fio = strs[0];
		cl.number = strs[1];
		cl.date_conclusion_contract = strs[2];
		cl.date_expired_contract = strs[3];
		clients.push_back(cl);
	}
}

void getStats() {
	string l;
	ifstream ifs("stats.txt");
	vector<string> subs;
	Stats st;
	while (!ifs.eof()) {
		getline(ifs, l);
		subs = split(l, ",");
		st.number = subs[0];
		st.code = atoi(subs[1].c_str());
		st.date_and_time_of_use = subs[2];
		st.time = atoi(subs[3].c_str());
		stats.push_back(st);
	}
}

void getServices(){
	string lin;
	ifstream ifs("serv.txt");
	vector<string> subs;
	Service s;
	while (!ifs.eof()){
		getline(ifs, lin);
		subs = split(lin, ",");
		s.name = subs[0];
		s.code = stoi(subs[1].c_str());
		s.tariff = atof(subs[2].c_str());
		s.time_span = subs[3];
		services.push_back(s);
	}
}


int getDay(string date)
{
	vector<string> v = split(date, ".");
	return stoi(v[0].c_str());
}


int getMonth(string date)
{
	vector<string> v = split(date, ".");
	return stoi(v[1].c_str());
}

int getYear(string date)
{
	vector<string> v = split(date, ".");
	return stoi(v[2].c_str());
}

Clients findClient(string number) {
	for (int i = 0; i < clients.size(); i++) {
		if (clients[i].number == number)
			return clients[i];
	}
}

Service findServ(int code) {
	for (int i = 0; i < services.size(); i++) {
		if (services[i].code == code)
			return services[i];
	}
}

void Time() {

}
int main(){
	setlocale(LC_ALL, "RUS");
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	int current_year = timeinfo->tm_year + 1900;
	int current_month = timeinfo->tm_mon + 1;
	int current_day = timeinfo->tm_mday;
	getClients();
	getStats();
	getServices();
	string sum;
	float costs;
	ifstream params("Param.ini");
	ofstream ofs("Report.txt");
	getline(params, sum);
	for (int i = 0; i < stats.size(); i++) {
		string iter = stats[i].date_and_time_of_use;
		if ((getYear(iter) == current_year) && (getMonth(iter) == current_month) && (current_day - getDay(iter) <= 6)) {
			if (findServ(stats[i].code).time_span == "���")
				costs = findServ(stats[i].code).tariff * (stats[i].time / 60);
			else if (findServ(stats[i].code).time_span == "�����")
				costs = findServ(stats[i].code).tariff * (stats[i].time / 86400);
			else if (findServ(stats[i].code).time_span == "�����")
				costs = findServ(stats[i].code).tariff * (stats[i].time / 2592000);
			else if (findServ(stats[i].code).time_span == "#")
				costs = findServ(stats[i].code).tariff;
			if (stoi(sum) <= costs) {
				ofs << findClient(stats[i].number).number << ", " << findClient(stats[i].number).date_conclusion_contract << endl;
			}
			else {
				cout << "��� ������" << endl;
				system("pause");
		}
		}
	}
	return 0;
}